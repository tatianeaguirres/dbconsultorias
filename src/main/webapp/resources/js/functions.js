/**
 * All javascript functions
 */
function alertFunction(message)
{
  alert(message);
}

function toggleDetails(image) {
	var detailsId = image.id.substring(0, image.id.lastIndexOf(':'))
			+ ':details';
	var details = document.getElementById(detailsId);
	var othersTable = document.getElementsByTagName("table");
	var othersIMG = document.getElementsByTagName("img");
	
	//change others table to display none
	for(var i=0;i<othersTable.length;i++){
		if(othersTable[i].id.indexOf("details") > -1 && othersTable[i].id != details.id){
			othersTable[i].style.display = 'none';
		}
	}
	//change others images to up
	for(var i=0;i<othersIMG.length;i++){
		if(othersIMG[i].id != image.id && othersIMG[i].currentSrc.indexOf("up") > -1){
			othersIMG[i].src = '/dbconsultorias/resources/img/down.png';
		}
	}
	
	if (details.style.display == 'none') {
		details.style.display = 'inline-table';
		image.src = '/dbconsultorias/resources/img/up.png';
	} else {
		details.style.display = 'none';
		image.src = '/dbconsultorias/resources/img/down.png';
	}
}