package br.com.dbserver.dbconsultorias.login;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import br.com.dbserver.dbconsultorias.users.User;

@ManagedBean
@SessionScoped
public class LoginMB implements Serializable {

    private static final long serialVersionUID = 3341048902559393106L;

    @ManagedProperty(value = "#{userSession}")
    private UserSession userSession;
    private String password;
    // private String email;
    private String username;
    private String msg;
    private UIComponent logar;
   

    @Inject
    private LoginRepository repository;

    private User user;

    private final String REDIRECT_TO_USER_HOME = "/index.xhtml?faces-redirect=true";
    private final String STAY_LOGIN = "/login.xhtml?faces-redirect=true";

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;

    }

    public String login() {
        if (userSession.register(username, password)) {
            return REDIRECT_TO_USER_HOME;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(
                "Senha ou us�rio incorretos");
        context.addMessage(null, msg);
        
        return STAY_LOGIN;

    }
    

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
