package br.com.dbserver.dbconsultorias.login;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import br.com.dbserver.dbconsultorias.users.User;

@ManagedBean(name = "userSession")
@SessionScoped
public class UserSession implements Serializable {

    private static final long serialVersionUID = -4674781309927588516L;
    private final String REDIRECT_TO_USER_HOME = "/index.xhtml?faces-redirect=true";
    private final String STAY_LOGIN = "/login.xhtml?faces-redirect=true";

    @Inject
    private LoginRepository repository;

    private User user;

    public boolean register(String username, String password) {
        this.user = repository.getUser(username, password);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    public String getUsername() {
        return user.getUsername();
    }

    public boolean isLogged() {
        if (user != null) {
            return true;
        }
        return false;
    }

}
