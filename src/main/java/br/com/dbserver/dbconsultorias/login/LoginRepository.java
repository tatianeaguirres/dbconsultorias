package br.com.dbserver.dbconsultorias.login;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.dbserver.dbconsultorias.users.User;

@Stateless
public class LoginRepository {

    private final String GET_USER_BY_USERNAME_AND_PASSWORD = "from User u where u.username = :username and u.password = :password";

    @PersistenceContext(unitName = "datasource")
    private EntityManager em;

    public User getUser(String username, String password) throws NoResultException {
        String hash = missingCryptographyMethod(password);

        TypedQuery<User> query = em.createQuery(GET_USER_BY_USERNAME_AND_PASSWORD, User.class)
                .setParameter("username", username).setParameter("password", hash);

        try {
            User user = query.getSingleResult();
            return user;
            } 
        catch (NoResultException e){
            return null;
        }
    }


    public String missingCryptographyMethod(String password) {
        // the return should contains the hash for password
        return password;
    }

}
