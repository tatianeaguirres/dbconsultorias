package br.com.dbserver.dbconsultorias.users;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.inject.Inject;

import br.com.dbserver.dbconsultorias.companies.CompanyRepository;
import br.com.dbserver.dbconsultorias.roles.Role;

@ManagedBean
public class UserMB {
    
    @Inject
    private UserRepository repository;

    @Inject
    private CompanyRepository companyRepository;

    private String name;
    private String email;
    private String phone;
    private String username;

    private Long companyid;

    public Long getCompanyid() {
        return companyid;
    }
   
    public void setCompanyid(Long companyid) {
        this.companyid = companyid;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getUsername() {
        return username;
    }

    public String getPhone() {
        return phone;
    }
    
    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void createCustomer(){
        User user = new User();
        /*String companyids = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap().get("companyid");*/
        System.out.println(getCompanyid());
        /*Company company = companyRepository.find(companyids);
        user.setCompany(company);
        user.setUsername(username);
        user.setName(name);
        user.setPhone(phone);
        user.setEmail(email);
        user.setPassword(new Encrypter().randomString(10));
        user.setRole(repository.findRole("cliente"));
        repository.create(user);*/
    }
    
    
    public List<User> getUsersByCompanyId() {
        return companyRepository.find(new Long(9999)).getListUsers();
    }
    
    public List<User> getAll() {
        return repository.findAll();
    }

    public List<Role> getAllRoles() {
        return repository.getAllRoles();
    }
}
