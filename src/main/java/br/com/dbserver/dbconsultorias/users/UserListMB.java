package br.com.dbserver.dbconsultorias.users;

import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;



@ManagedBean
@SessionScoped
public class UserListMB {

    private UserRepository userRepository;
    private List<User> users;

    @Inject
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getUsers() {
        return users;
    }

}
