package br.com.dbserver.dbconsultorias.users;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.dbserver.dbconsultorias.generics.GenericDAO;
import br.com.dbserver.dbconsultorias.roles.Role;

@Stateless
public class UserRepository extends GenericDAO<User>{
    
    private final String GET_ROLE_BY_NAME = "from Role r where r.name = :name";
    
    private final String GET_USERS_BY_COMPANY = "from User u where u.company_id = :companyid";

    private final String USERNAME_IS_UNIQUE = "SELECT count(u.username) FROM User u WHERE u.username = :username";
    private final String EMAIL_IS_UNIQUE = "SELECT count(u.email) FROM User u WHERE u.email = :email";
    
    @PersistenceContext(unitName = "datasource")
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public UserRepository() {
        super(User.class);
    }
    
	public List<Role> getAllRoles() {
    	return getEntityManager().createQuery("from Role", Role.class).getResultList();
    }

    public Role findRole(String name) {
        TypedQuery<Role> query = getEntityManager().createQuery(GET_ROLE_BY_NAME, Role.class)
                .setParameter("name", name);
        return query.getSingleResult();
    }

    public Long countDuplicityUsername(User user){
        Query query = getEntityManager().createQuery(USERNAME_IS_UNIQUE, User.class).setParameter("username", user.getUsername());
        return (Long)query.getSingleResult();
    }
    
    public boolean validateUsername(User user){
        long counter = countDuplicityUsername(user);
        return (counter > 0)? false : true;
    }

    public Long countDuplicityEmail(User user){
        Query query = getEntityManager().createQuery(EMAIL_IS_UNIQUE, User.class).setParameter("email", user.getEmail());
        return (Long)query.getSingleResult();
    }
    
    public boolean emailIsValid(User user){
        long counter = countDuplicityUsername(user);
        return (counter > 0)? false : true;
    }
}
