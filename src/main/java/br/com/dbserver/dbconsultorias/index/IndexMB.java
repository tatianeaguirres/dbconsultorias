package br.com.dbserver.dbconsultorias.index;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import br.com.dbserver.dbconsultorias.login.UserSession;

@ManagedBean
@ViewScoped
public class IndexMB {

    private String page;

    @ManagedProperty(value = "#{userSession}")
    private UserSession userSession;

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    @PostConstruct
    public void init() {
    	page = "listcompany/listcompany"; // Default include.
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public UserSession getUserSession() {
        return userSession;
    }

}
