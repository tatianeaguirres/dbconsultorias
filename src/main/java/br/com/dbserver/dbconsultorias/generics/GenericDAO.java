/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbserver.dbconsultorias.generics;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public abstract class GenericDAO<T> {
    private Class<T> entityClass;

    public GenericDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public T create(T entity) {
        getEntityManager().persist(entity);
        return entity;
    }

    public T edit(T entity) {
        getEntityManager().merge(entity);
        return entity;
    }

    public T remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
        return entity;
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        Query query = getEntityManager().createQuery("from " + entityClass.getName());
        return query.getResultList();
    }

    public int count() {
        return findAll().size();
    }

}
