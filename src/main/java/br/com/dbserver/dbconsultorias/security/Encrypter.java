package br.com.dbserver.dbconsultorias.security;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.mindrot.jbcrypt.BCrypt;

public class Encrypter {

	public String encryptString(String string_not_hashed) {
		String salt = BCrypt.gensalt();
		String hashed_password = BCrypt.hashpw(string_not_hashed, salt);
		return hashed_password;
	}

	public String randomString(int size) {
		SecureRandom random = new SecureRandom();
		return new BigInteger(size, random).toString(32);
	}
}
