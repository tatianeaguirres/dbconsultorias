package br.com.dbserver.dbconsultorias.companies;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.dbserver.dbconsultorias.generics.GenericDAO;
import br.com.dbserver.dbconsultorias.users.User;

@Stateless
public class CompanyRepository extends GenericDAO<Company> {

    @PersistenceContext(unitName = "datasource")
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CompanyRepository() {
        super(Company.class);
    }

    // test
    public Company createTest(Company entity) {
        User user = new User("test@te", "123");
        User user2 = new User("test@test", "123");
        user.setCompany(entity);
        user2.setCompany(entity);
        entity.addUser(user);
        entity.addUser(user2);
        getEntityManager().persist(user);
        getEntityManager().persist(user2);
        getEntityManager().persist(entity);
        return entity;
    }

    public Company create(Company company){
    	getEntityManager().persist(company);
    	getEntityManager().flush();
    	return company;
    }
    
    public Company createWithUser(Company company, User user) {
        user.setCompany(company);
        company.addUser(user);
        getEntityManager().persist(user);
        getEntityManager().persist(company);
        return company;
    }

    public void removeUserFromCompany(Company c, User u) {
        u.setCompany(null);
        c.removeUser(u);
        getEntityManager().remove(getEntityManager().merge(u));
    }

}
