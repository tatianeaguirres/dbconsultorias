package br.com.dbserver.dbconsultorias.companies;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import br.com.dbserver.dbconsultorias.users.User;

@ManagedBean
@RequestScoped
public class CompanyMB {
	
    @Inject
    private CompanyRepository companyRepository;
    
    private Long id;
    private String name;
    private String address;
    private String phone;
    private String cnpj;
    private String cep;
    private String city;
    private String msg;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    
    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @PostConstruct
    public void initialize() {
    }

    public List<Company> getAll() {
        return companyRepository.findAll();
    }

    public void removeUserFromCompany(Company c, User u) {
        companyRepository.removeUserFromCompany(c, u);
    }
    
    public void removeCompany(Company c) {
        if(c.getListUsers().isEmpty()){
            companyRepository.remove(c);
            
        }else{
           
        }
    }

    public void save() throws IOException {
        Company entity = new Company(getName(), getCnpj(), getPhone(), getAddress(), getCep(), getCity());
        companyRepository.create(entity);
        String url = "createContact.xhtml?companyid="+entity.getId(); 
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
