package br.com.dbserver.dbconsultorias.companies;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import br.com.dbserver.dbconsultorias.users.User;

@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String name;
    
    @Column(nullable = false)
    private String cnpj;
    
    @Column(nullable = false)
    private String address;
    
    @Column(nullable = false)
    private String phone;
    
    @Column(nullable = false)
    private String cep;
    
    @Column(nullable = false)
    private String city;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "company")
    private List<User> listUsers;
    
    public Company() {
    }

    public Company(String name, String cnpj, String phone, String address, String cep, String city) {
        setName(name);
        setCnpj(cnpj);
        setPhone(phone);
        setAddress(address);
        setCep(cep);
        setCity(city);
        this.listUsers = new ArrayList<User>();
    }

    public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<User> getListUsers() {
        return listUsers;
    }

    public void addUser(User user) {
        listUsers.add(user);
    }

    public void removeUser(User user) {
        listUsers.remove(user);
    }

}
